<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBorrowedBookTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('borrowed_book', function (Blueprint $table) {
            $table->id();
            $table->foreignId("patron_id")->bigInteger()->references("id")->on("patrons");
            $table->smallInteger("copies");
            $table->foreignId("book_id")->bigInteger()->references("id")->on("books");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('borrowed_book');
    }
}
