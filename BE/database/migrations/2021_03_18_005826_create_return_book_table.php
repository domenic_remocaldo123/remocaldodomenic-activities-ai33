<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReturnBookTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('return_book', function (Blueprint $table) {
            $table->id();
            $table->foreignId("book_id")->bigInteger()->references("id")->on("books");
            $table->smallInteger("copies");
            $table->foreignId("patron_id")->bigInteger()->references("id")->on("patrons");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('return_book');
    }
}
