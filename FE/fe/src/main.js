import Vue from 'vue';
import router from '../src/routes/router';
import App from '../src/App.vue';
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue';
Vue.use(BootstrapVue);
Vue.use(IconsPlugin);
Vue.use(BootstrapVue)
Vue.use(IconsPlugin)
import './assets/css/design.css'
import './assets/css/responsive.css'
import './assets/css/style.css'
Vue.config.productionTip = false
new Vue({
  router,
  render: (h) => h(App),
}).$mount('#app')
